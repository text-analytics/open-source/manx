FROM python:3.8-slim-buster
RUN apt-get update
RUN apt-get install -y --no-install-recommends curl

RUN python -m venv /opt/venv
# Make sure we use the virtualenv:
ENV PATH="/opt/venv/bin:$PATH"

ENV POETRY_VERSION="1.1.4"
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py > get-poetry.py
RUN python get-poetry.py --version $POETRY_VERSION
ENV PATH="/root/.poetry/bin:$PATH"

# Poetry will try to create a virtualenv by default, but we already made one, so we disable that behavior
ENV POETRY_VIRTUALENVS_CREATE=false
COPY poetry.lock pyproject.toml ./
RUN poetry install
