from collections import namedtuple
from typing import List
from unittest.mock import ANY, AsyncMock, MagicMock, Mock

import pytest
from elasticsearch.exceptions import ElasticsearchException
from pytest_mock import MockerFixture

from manx.exceptions import ManxException
from manx.files import get_python_files
from manx.migrate import (
    _apply,
    _new_doc_generator,
    create_new_index,
    migrate_index,
    migration_required,
    next_migration_generator,
)

file_under_test = "manx.migrate"
# All tests are async and will be awaited
pytestmark = pytest.mark.asyncio

alias = "test-alias"
old_index_name = "old-test-index"
package = "tests.migrations"
MockMigration = namedtuple("MockMigration", ["name", "stamp", "hash_"])


@pytest.fixture(scope="module")
def test_migrations():
    return list(get_python_files(package))


@pytest.fixture()
def mock_migration():
    mock = AsyncMock()
    # Need to force a normal (sync) mock for to_meta_doc
    mock.to_meta_doc = Mock()
    return mock


async def test_create_new_index(mocker, test_migrations):
    mock_es = AsyncMock()
    mock_es.search.return_value = {"hits": {"hits": []}}
    await create_new_index(alias, package, mock_es)

    alpha_config = await test_migrations[0].module.configuration({})
    gamma_config = await test_migrations[1].module.configuration(alpha_config)
    mock_es.indices.create.assert_awaited_once_with(
        index=f"{alias}-3", body=gamma_config
    )
    mock_es.indices.put_alias.assert_awaited_once()
    assert mock_es.index.await_count == 2


async def test_create_new_index_custom(mocker):
    mock_es = AsyncMock()
    mock_es.search.return_value = {"hits": {"hits": []}}
    custom_config = {"hello": "world", "foo": "bar", "custom": "value"}
    await create_new_index(alias, package, mock_es, custom_config)

    mock_es.indices.create.assert_awaited_once_with(
        index=f"{alias}-3", body=custom_config
    )
    mock_es.indices.put_alias.assert_awaited_once()
    assert mock_es.index.await_count == 2


async def test_migrate(mocker):
    mock_apply = mocker.patch(f"{file_under_test}._apply")
    mock_es = AsyncMock()
    mock_es.search.return_value = {"hits": {"hits": []}}
    await migrate_index(alias, package, mock_es)

    assert mock_apply.await_count == 2


@pytest.mark.parametrize(
    "migration_files,applied_migrations,expected_result",
    [
        (
            [MockMigration("a", 1, "0001"), MockMigration("b", 2, "0002")],
            [MockMigration("a", 1, "0001"), MockMigration("b", 2, "0002")],
            False,
        ),
        (
            [MockMigration("a", 1, "0001"), MockMigration("b", 2, "0002")],
            [MockMigration("a", 1, "0001")],
            True,
        ),
        (
            [MockMigration("a", 1, "0001"), MockMigration("b", 2, "0002")],
            [],
            True,
        ),
    ],
)
async def test_migration_required(
    mocker: MockerFixture,
    migration_files: List[MockMigration],
    applied_migrations: List[MockMigration],
    expected_result: bool,
):
    mock_es = MagicMock()
    mock_get_python_files = mocker.patch(f"{file_under_test}.get_python_files")
    mock_get_python_files.return_value = iter(migration_files)
    mock_metadata_get = mocker.patch(
        f"{file_under_test}.metadata.get", new_callable=AsyncMock
    )
    mock_metadata_get.return_value = applied_migrations

    actual_result = await migration_required(alias, package, mock_es)

    assert actual_result == expected_result


async def test_next_migration_to_apply_warnings(mocker):
    mock_warn = mocker.patch(f"{file_under_test}.log.warn")
    migrations = [
        MockMigration("b", 2, "0010"),
        MockMigration("d", 4, "four"),
        MockMigration("e", 5, "0101"),
    ]
    previously_applied = [
        MockMigration("a", 1, "0001"),
        MockMigration("c", 3, "0011"),
        MockMigration("d", 4, "0100"),
        MockMigration("e", 5, "0101"),
        MockMigration("f", 6, "0110"),
    ]

    gen = next_migration_generator(iter(migrations), iter(previously_applied))

    with pytest.raises(StopIteration):
        next(gen)
    assert mock_warn.call_count == 5


async def test_next_migration_to_apply_success(mocker):
    mock_info = mocker.patch(f"{file_under_test}.log.info")
    mock_warn = mocker.patch(f"{file_under_test}.log.warn")
    migrations = [
        MockMigration("a", 1, "0001"),
        MockMigration("b", 2, "0010"),
        MockMigration("c", 3, "0011"),
        MockMigration("d", 4, "0100"),
    ]
    previously_applied = [
        MockMigration("a", 1, "0001"),
        MockMigration("b", 2, "0010"),
    ]

    gen = next_migration_generator(iter(migrations), iter(previously_applied))
    assert next(gen) is migrations[2]
    assert next(gen) is migrations[3]
    with pytest.raises(StopIteration):
        next(gen)

    mock_warn.assert_not_called()
    assert mock_info.call_count == 4


async def test__apply(mocker, mock_migration):
    mock_async_bulk = mocker.patch(f"{file_under_test}.async_bulk")
    mock_es = AsyncMock()
    mock_es.indices.get_alias.return_value = [old_index_name]

    await _apply(alias, mock_migration, {}, mock_es)

    mock_es.indices.add_block.assert_awaited_once_with(old_index_name, "write")
    mock_async_bulk.assert_awaited_once_with(mock_es, ANY)
    mock_es.indices.create.assert_awaited_once()
    mock_es.indices.delete.assert_awaited_once_with(old_index_name)


async def test__apply_bootstrap(mocker, mock_migration):
    mock_async_bulk = mocker.patch(f"{file_under_test}.async_bulk")
    mock_es = AsyncMock()
    mock_es.indices.get_alias.return_value = [alias]

    await _apply(alias, mock_migration, {}, mock_es)

    mock_async_bulk.assert_awaited_once_with(mock_es, ANY)
    mock_es.indices.create.assert_awaited_once()
    mock_es.indices.delete.assert_awaited_once_with(alias)


async def test__apply_rollback(mocker, mock_migration):
    mock_error = mocker.patch(f"{file_under_test}.log.error")
    mock_es = AsyncMock()
    mock_es.indices.get_alias.return_value = [alias]
    mock_async_bulk = mocker.patch(f"{file_under_test}.async_bulk")
    mock_async_bulk.side_effect = ElasticsearchException

    with pytest.raises(ElasticsearchException):
        await _apply(alias, mock_migration, {}, mock_es)

    mock_es.indices.create.assert_awaited_once()
    mock_es.indices.put_settings.assert_awaited_once()
    mock_es.indices.delete.assert_awaited_once()
    assert mock_error.call_count == 3


async def test__apply_verify(mocker, test_migrations):
    mock_async_bulk = mocker.patch(f"{file_under_test}.async_bulk")
    migration = test_migrations[0]
    mock_es = AsyncMock()
    mock_es.indices.get_alias.return_value = [old_index_name]

    await _apply(alias, migration, {}, mock_es)

    mock_es.indices.create.assert_awaited_once()
    mock_es.indices.get_alias.assert_awaited_once_with(alias)
    mock_async_bulk.assert_awaited_once()


async def test__apply_verify_rollback(mocker, test_migrations):
    mock_error = mocker.patch(f"{file_under_test}.log.error")
    mock_async_bulk = mocker.patch(f"{file_under_test}.async_bulk")
    migration = test_migrations[1]
    mock_es = AsyncMock()
    mock_es.indices.get_alias.return_value = [old_index_name]

    with pytest.raises(ManxException):
        await _apply(alias, migration, {}, mock_es)

    mock_es.indices.create.assert_awaited_once()
    mock_es.indices.get_alias.assert_awaited_once_with(alias)
    mock_es.indices.add_block.assert_awaited_once_with(old_index_name, "write")
    mock_async_bulk.assert_awaited_once()
    mock_es.indices.put_settings.assert_awaited_once()
    mock_es.indices.delete.assert_awaited_once()
    assert mock_error.call_count == 3


async def test__new_doc_generator(mocker, test_migrations):
    mock_async_scan = mocker.patch(f"{file_under_test}.async_scan")

    async def mock_gen():
        yield {"_source": "test_source_1"}
        yield {"_source": "test_source_2"}

    mock_async_scan.return_value = mock_gen()
    mock_new_index_name = "mock_new_index_name"
    mock_migration = test_migrations[0]
    mock_es = AsyncMock()

    gen = _new_doc_generator(alias, mock_new_index_name, mock_migration, mock_es)
    async for d in gen:
        d.__setitem__.assert_called_with("_index", mock_new_index_name)

    assert mock_es.do_stuff.await_count == 2
    mock_async_scan.assert_called_once()
