import pytest

from manx.configs import get_all_configs, get_config
from manx.files import MigrationFile

# Tell pytest that every test should be run asynchronously
pytestmark = pytest.mark.asyncio

package = "tests.migrations"


async def test_get_config():
    expected = {"hello": "world", "foo": "bar"}
    config = await get_config(package)

    assert expected == config


async def test_get_config_default():
    expected = {}
    config = await get_config("tests")

    assert expected == config


async def test_get_all_configs():
    expected = {1: {"hello": "world"}, 3: {"hello": "world", "foo": "bar"}}
    config = await get_all_configs(package)

    assert expected == config


async def test_get_all_configs_last_applied():
    last_applied = MigrationFile(1, "test", "", config={"o": "hai"})
    expected = {1: {"o": "hai"}, 3: {"o": "hai", "foo": "bar"}}
    config = await get_all_configs(package, last_applied)

    assert expected == config
