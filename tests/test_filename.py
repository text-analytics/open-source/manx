import pytest

from manx.exceptions import ManxException
from manx.files import get_python_files, migration_from_file, parse_file_name


@pytest.mark.parametrize(
    "test_filename,expected",
    [
        ("1519669955912-first.py", [1519669955912, "first"]),
        ("1519669956912_Other.py", [1519669956912, "Other"]),
        ("1Other_thing_to_do.py", [1, "Other_thing_to_do"]),
        ("2018022601.py", [2018022601, None]),
    ],
)
def test_hyphen(test_filename, expected):
    stamp, label = parse_file_name(test_filename)
    assert stamp == expected[0]
    assert label == expected[1]


@pytest.mark.parametrize(
    "test_filename",
    [
        "Nope.py",
        "Comment_first-20180226.py",
        "1 23-space.py",
        "123 -space.py",
        "123- space.py",
        "123-sp ace.py",
    ],
)
def test_fails(test_filename):
    val = parse_file_name(test_filename)
    assert val is None


@pytest.mark.parametrize(
    "match, package, directory, filename, posix_hash",
    [
        (
            (1, "alpha"),
            "tests.migrations",
            "tests/migrations",
            "1_alpha.py",
            "d6382989968872dbfe9fa6a677e9133d843cc087087f2be9466aa2cf",
        ),
        (
            (3, "gamma"),
            "tests.migrations",
            "tests/migrations",
            "3-gamma.py",
            "b58123506fd70acae5dda9295a9ca7996463a619a39206a92b3edc2c",
        ),
    ],
)
def test_migration_from_file(mocker, match, package, directory, filename, posix_hash):
    mock_import_module = mocker.patch("importlib.import_module")
    mf = migration_from_file(match, package, directory, filename)
    assert mf.stamp == match[0]
    assert mf.name == match[1]
    assert mf.hash_ == posix_hash
    mock_import_module.assert_called_once()


@pytest.mark.parametrize(
    "test_filenames,expected",
    [
        (["1-a.py", "2-b.py", "10-c.py"], [1, 2, 10]),
        (["007.py", "06.py", "5.py"], [5, 6, 7]),
        (["101.py", "1002.py", "10003.py"], [101, 1002, 10003]),
    ],
)
def test_get_python_files_sorting(mocker, test_filenames, expected):
    mocker.patch("importlib.import_module")
    mocker.patch("manx.files._hash")

    mock_listdir = mocker.patch("os.listdir")
    mock_listdir.return_value = test_filenames

    migration_files = list(get_python_files("test.mocked"))
    stamps = [mf.stamp for mf in migration_files]

    assert stamps == expected


def test_get_python_files_two_stamps(mocker):
    mocker.patch("importlib.import_module")
    mocker.patch("manx.files._hash")

    mock_listdir = mocker.patch("os.listdir")
    mock_listdir.return_value = ["4-foo.py", "4-bar.py"]

    with pytest.raises(ManxException):
        get_python_files("test.mocked")
