async def configuration(previous_config):
    return {
        **previous_config,
        "foo": "bar"
    }


async def up_doc(es, doc):
    return await es.other_things("hi")


async def verify(es):
    return False
