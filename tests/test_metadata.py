from collections import namedtuple
from unittest.mock import AsyncMock

import pytest

from manx import metadata

# All tests are async and will be awaited
pytestmark = pytest.mark.asyncio

alias = "test-alias"
MockMigration = namedtuple("MockMigration", ["name", "stamp", "sha3", "config"])


async def test_get():
    a = MockMigration("a", 1, "0001", {})
    b = MockMigration("b", 2, "0010", {})
    c = MockMigration("c", 3, "0011", {})
    d = MockMigration("d", 4, "0100", {})
    e = MockMigration("e", 5, "0101", {})
    f = MockMigration("f", 6, "0110", {})
    mock_es = AsyncMock()

    mock_es.search.return_value = {
        "hits": {
            "hits": [
                {"_source": b._asdict()},
                {"_source": f._asdict()},
                {"_source": a._asdict()},
                {"_source": c._asdict()},
                {"_source": e._asdict()},
                {"_source": d._asdict()},
            ]
        }
    }

    migrations = await metadata.get(alias, mock_es)
    migration_stamps = [r.stamp for r in migrations]

    assert migration_stamps == [1, 2, 3, 4, 5, 6]
